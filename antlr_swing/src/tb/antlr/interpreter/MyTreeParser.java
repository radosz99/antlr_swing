package tb.antlr.interpreter;

import tb.antlr.symbolTable.LocalSymbols;
import java.util.Optional;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

public class MyTreeParser extends TreeParser {
	
	protected LocalSymbols symbols = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }
 
    protected int calculateRoot(int number, int degree) {
    	return (int) Math.pow(number, 1.0 / degree);
    }
    
    protected int calculatePower(int number, int degree) {
    	return (int) Math.pow(number, degree);
    }
    
    protected int calculateModulo(int a, int b) {
    	return a % b;
    }
    
    protected void print(Object o) {
    	Optional<Object> opt = Optional.ofNullable(o);
    	if(opt.isPresent()) {
        	System.out.println(opt.get().toString());
    	}
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
}
